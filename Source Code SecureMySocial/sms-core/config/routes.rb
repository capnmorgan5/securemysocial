Rails.application.routes.draw do
  namespace :admin do
    resources :groups do
      collection do
        post "/rules", :action => :add_system_rule
        match "/cron", :action => :system_cron, via: [:get, :post]
      end
      member do
        get "/reports", :action => :reports
        get "/alerts", :action => :alerts

        get "/rules", :action => :rules
        post "/rules", :action => :save_rules

        post "/import", :action => :import

        get "/users", :action => :users
        post "/users", :action => :save_users

        get "/options", :action => :options
        patch "/options", :action => :save_options

        post "/sysrules", :action => :update_group_system_rules
        post "/subgroup", :action => :add_subgroup
      end
    end
  end

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  get "/profile" => "profile#index", :as => :profile
  get "/alerts" => "profile#alerts", :as => :user_alerts
  patch "/profile" => "profile#update"

  get "/login/:provider" => "home#login", :as => :login

  get "/join/:token" => "groups#add_user"
  get "/join" => "home#join"

  root to: 'home#index'
end
