class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    provider :facebook
  end

  def twitter
    provider :twitter
  end

  def linkedin
    provider :linkedin
  end

  private
  def provider provider_id
	print "||||||||||||||||||||||||||||||||||||||||||||||||||| -- #{provider_id}"
	print "||||||||||||||||||||||||||||||||||||||||||||||||||| -- #{provider_id}"

    @user = User.provider(request.env["omniauth.auth"], provider_id, current_user.id).user
    session["devise.#{provider_id}_data"] = request.env["omniauth.auth"]
    flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => provider_id.to_s.titleize

    sign_in_and_redirect @user.reload, :event => :authentication
  end
end
