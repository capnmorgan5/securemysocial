class GroupsController < ApplicationController

  def add_user
    member_record = Member.where(token: params[:token]).first
    if member_record
      remember_the_token(member_record)
      sign_out :user
      redirect_to "/join"
    else
      redirect_to root_path
    end
  end

  private
  def remember_the_token(member_record)
    session[:sms_valid_token] = member_record.token
    session[:sms_role] = member_record.role
    session[:sms_email] = member_record.invite_email
  end

end
