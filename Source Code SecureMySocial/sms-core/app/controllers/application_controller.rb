class ApplicationController < ActionController::Base
  before_action :set_redirect, :only => [:index, :show], :if => -> { current_user.blank? && !request.xhr? }
  before_action :join_check, :unless => -> { current_user }
  before_action :account_ok, :if => -> { current_user.present? && !request.xhr? }
  before_action :set_branding

  expose(:devise_controller) { params[:controller] =~ /^devise/ }
  expose(:current_group) { Group.where(domain: request.host).first || (current_user ? current_user.groups.first : nil) }

  private

  def set_redirect
    session[:redirect] = request.fullpath
  end

  def join_check
    redirect_to "/join" if session[:sms_role] == 'user' && params[:action] !~ /(join|twitter|facebook)/ && !devise_controller
  end

  def set_branding
    if current_group
      parent_group = current_group.parent
      @page_title = current_group.title.presence
      @page_title ||= parent_group.title.presence if parent_group
      @page_description = current_group.description.presence
      @page_description ||= parent_group.description.presence if parent_group
      @account_name = current_group.name.presence
      @account_name ||= parent_group.name.presence if parent_group
    end
    @page_title ||= "Secure My Social"
    @page_description ||= "Secure My Social"
    @account_name ||= "SecureMySocial"
  end

  def account_ok
    redirect_to profile_path if current_user.missing_email? && params[:controller] !~ /(profile|devise)/
  end

  def authenticate_admin!
    redirect_to root_path unless current_user && current_user.system_admin?
  end

  def wcu(collection)
    collection.merge created_by: current_user.id
  end

  def after_sign_in_path_for(resource)
    redirect = session[:redirect]
    if session[:sms_valid_token]
      member = Member.join(current_user, session[:sms_valid_token])
      session[:sms_role] = nil
      session[:sms_valid_token] = nil
      if member.role == 'admin'
        admin_group_path(member.group)
      else
        redirect = session[:redirect]
        redirect.presence || root_path
      end
    else
      session[:redirect] = nil
      redirect.presence || root_path
    end
  end
end
