class ProfileController < ApplicationController
  before_action :authenticate_user!

  expose(:self_group) { current_user.self_group }

  def index
    @user = current_user
    @providers = @user.providers
    @has_twitter = @providers.where(provider: 'twitter').count > 0
    @has_facebook = @providers.where(provider: 'facebook').count > 0
    @has_linkedin = @providers.where(provider: 'linkedin').count > 0
    @personal_rules = Rule.personal.global.with_selection_for_group(self_group.id).order(created_at: :desc)
    @career_rules = Rule.career.global.with_selection_for_group(self_group.id).order(created_at: :desc)
    @financial_rules = Rule.financial.global.with_selection_for_group(self_group.id).order(created_at: :desc)
  end

  def update
    @group = self_group
    current_rules_ids = @group.system_rules.pluck(:source_rule_id)
    puts "Current rules ids: #{current_rules_ids}"
    new_rules_ids = []
    if params[:sysrules].present?
      new_rules_ids = params[:sysrules].keys.map(&:to_i)
      puts "New Rules Ids #{new_rules_ids}"
      on_rules = new_rules_ids - current_rules_ids
      puts "On Rules #{on_rules}"
      Rule.find(on_rules).each do |rule|
        options = rule.slice(:name, :message, :description, :patterns, :rule_type, :quarantine,
                             :send_author, :send_admins, :include_post_content, :include_restore_link,
                             :phishing, :send_phishing_admins, :fraud, :send_fraud_admins)
        @group.system_rules << Rule.create(options.merge group_id: @group.id, source_rule_id: rule.id)
      end
    end
    off_rules = current_rules_ids - new_rules_ids
    puts "Off Rules #{off_rules}"
    if off_rules.present?
      @group.system_rules.where(source_rule_id: off_rules).soft_delete
    end

    @user = current_user
    @user.update_attributes member_params
    if @user.save
      flash[:notice] = "Profile updated"
      redirect_to profile_path
    else
      render :index
    end
  end

  def alerts
    @alerts = Alert.where(user_id: current_user.id).includes(:rule).order(created_at: :desc)
  end

  private
  def member_params
    params[:member].permit(:email, :first_name, :last_name, :phone, :alert_email, :alert_phone, :auto_delete)
  end
end
