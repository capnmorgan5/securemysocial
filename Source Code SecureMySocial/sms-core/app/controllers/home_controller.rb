class HomeController < ApplicationController

  def index
    if current_user
      if current_user.system_admin?
        return redirect_to admin_groups_path
      end

      if (group = current_user.admin_groups.first)
        return redirect_to admin_group_path(group)
      end

      if (group = current_user.phishing_admin_groups.first)
        return redirect_to reports_admin_group_path(group)
      end

      if (group = current_user.fraud_admin_groups.first)
        return redirect_to reports_admin_group_path(group)
      end
    else
      return redirect_to new_user_session_path
    end

    redirect_to profile_path
  end

  def join
    redirect_to "/" unless session[:sms_valid_token].present?

    @email = session[:sms_email]

    @hide_login = true
  end

  def login
    redirect_to omniauth_authorize_path(:user, params[:provider].to_sym)
  end

end
