class Admin::GroupsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin!, only: [:index, :create, :update, :add_system_rule, :system_cron]
  before_action :set_admin_group, only: [:reports, :alerts]

  def index
    @top_level_groups = Group.top_level.with_counts.order(created_at: :desc)
    @system_rules = Rule.system.global.order(created_at: :desc)
    @personal_rules = Rule.personal.global.order(created_at: :desc)
    @career_rules = Rule.career.global.order(created_at: :desc)
    @financial_rules = Rule.financial.global.order(created_at: :desc)
    @business_rules = Rule.business.global.order(created_at: :desc)

    @builtin_rules = { system: @system_rules, career: @career_rules, personal: @personal_rules, financial: @financial_rules }
  end

  def show
    @group = current_user.admin_groups.find(params[:id])
    @sub_groups = @group.sub_groups

    @past_24h_alerts = @group.alerts.past_24h.count
    @past_24h_class = @past_24h_alerts > 0 ? "danger" : "success"
    @total_users = @group.regular_users.count
    @total_alerts = @group.alerts.count
    @total_facebook_accounts = @group.regular_users.facebook.count
    @total_twitter_accounts = @group.regular_users.twitter.count
  end

  def users
    @group = current_user.admin_groups.find(params[:id])
    @users = {}
    if @group.phishing
      @users['phishing_manager'] = @group.phishing_managers.order(created_at: :desc)
    end
    if @group.fraud
      @users['fraud_manager'] = @group.fraud_managers.order(created_at: :desc)
    end
    @users['user'] = @group.regular_users.order(created_at: :desc)
    @users['admin'] = @group.admin_users.order(created_at: :desc)
    @invited_members = Member.pending.where(group_id: @group.id).order(created_at: :desc)
  end

  def save_users
    @group = current_user.admin_groups.find(params[:id])
    if @group.maxed?
      return redirect_to(users_admin_group_path(@group.id), alert: "No more users can be added to this account")
    end
    member = Member.new(wcu(group_member_params).merge(role: params[:role]))
    @group.members << member
    if @group.save
      UserMailer.invite_user_to_group(member, @group).deliver_later
      flash[:notice] = "#{params[:role].capitalize} account was added successfully"
      redirect_to users_admin_group_path(@group, anchor: params[:role])
    else
      @admin_users = @regular_users = @invited_admins = @invited_users = []
      render :users
    end
  end

  def import
    @group = current_user.admin_groups.find(params[:id])
    @import = @group.imports.build(import_params.merge(user_id: current_user.id))
    if @import.has_valid_content? && @import.save
      flash[:notice] = "Import was saved and queued for processing"
    else
      flash[:alert] = "Could not sove this import"
    end
    redirect_to users_admin_group_path(@group, anchor: 'import')
  end

  def rules
    @group = current_user.admin_groups.find(params[:id])
    @business_rules = @group.rules.business.order(created_at: :desc)

    business_rules_sources = @business_rules.pluck(:source_rule_id)
    Rule.business.global.each do |rule|
      unless business_rules_sources.include?(rule.id)
        options = rule.slice(:name, :message, :description, :patterns, :rule_type, :quarantine,
                             :send_author, :send_admins, :include_post_content, :include_restore_link,
                             :phishing, :send_phishing_admins, :fraud, :send_fraud_admins)
        @group.rules << Rule.create(options.merge group_id: @group.id, source_rule_id: rule.id)
      end
    end

    @custom_rules = @group.rules.custom.non_system.order(created_at: :desc)
    @exception_rules = @group.rules.exception.non_system.order(created_at: :desc)
    @system_rules = Rule.system.global.with_selection_for_group(@group.id).order(created_at: :desc)
  end

  def reports
  end

  def alerts
    alerts = Alert.where(group_id: @group.id).joins(:rule).select("alerts.*, rules.name").order(created_at: :desc)
    if @role == 'phishing_manager'
      alerts = alerts.where(phishing: true)
    end
    if @role == 'fraud_manager'
      alerts = alerts.where(fraud: true)
    end
    alerts = alerts.map do |ar|
      {
        accountInfo: "#{ar.screen_name.present? ? 'Twitter' : 'Facebook'}: #{ar.screen_name.presence || ar.provider_from_name}",
        accountEmail: ar.author_email,
        accountPhone: ar.author_phone,
        postWrittenAt: ar.message_time.to_s(:db),
        ruleName: ar.name,
        postMessage: ar.message,
        phishingRelated: ar.phishing ? "Yes" : "",
        fraudRelated: ar.fraud ? "Yes" : "",
        postDeletedAt: ar.post_deleted_at.presence && ar.post_deleted_at.to_s(:time)
      }
    end

    render json: alerts
  end

  def save_rules
    @group = current_user.admin_groups.find(params[:id])
    params[:rule][:quarantine] = false unless params[:rule][:quarantine].present?
    params[:rule][:send_author] = false unless params[:rule][:send_author].present?
    params[:rule][:send_admins] = false unless params[:rule][:send_admins].present?
    params[:rule][:phishing] = false unless params[:rule][:phishing].present?
    params[:rule][:send_phishing_admins] = false unless params[:rule][:send_phishing_admins].present?
    params[:rule][:fraud] = false unless params[:rule][:fraud].present?
    params[:rule][:send_fraud_admins] = false unless params[:rule][:send_fraud_admins].present?
    params[:rule][:include_post_content] = false unless params[:rule][:include_post_content].present?
    params[:rule][:include_restore_link] = false unless params[:rule][:include_restore_link].present?
    if params[:rule] && params[:rule][:id]
      rule = @group.rules.active.find(params[:rule][:id])
      raise "invalid request" unless rule
      rule.attributes = rule_params
      rule.save!
    else
      rule = Rule.new(wcu(rule_params))
      @group.rules << rule
    end
    if @group.save
      flash[:notice] = "Rule was updated successfully"
      redirect_to rules_admin_group_path(@group, anchor: rule.anchor)
    else
      @rules = @system_rules = @business_rules = []
      render :show
    end
  end

  def add_system_rule
    params[:rule][:quarantine] = false unless params[:rule][:quarantine].present?
    params[:rule][:send_author] = false unless params[:rule][:send_author].present?
    params[:rule][:send_admins] = false unless params[:rule][:send_admins].present?
    params[:rule][:phishing] = false unless params[:rule][:phishing].present?
    params[:rule][:send_phishing_admins] = false unless params[:rule][:send_phishing_admins].present?
    params[:rule][:fraud] = false unless params[:rule][:fraud].present?
    params[:rule][:send_fraud_admins] = false unless params[:rule][:send_fraud_admins].present?
    params[:rule][:include_post_content] = false unless params[:rule][:include_post_content].present?
    params[:rule][:include_restore_link] = false unless params[:rule][:include_restore_link].present?
    if params[:rule] && params[:rule][:id]
      rule = Rule.find(params[:rule][:id])
      rule.attributes = rule_params
      notice = "Rule was updated successfully"
    else
      rule = Rule.new(wcu(rule_params))
      notice = "Rule was added successfully"
    end
    if rule.save
      flash[:notice] = notice
      redirect_to admin_groups_path
    else
      @top_level_groups = @system_rules = @business_rules = []
      render :show
    end
  end

  def add_subgroup
    group = current_user.admin_groups.find(params[:id])
    new_group = Group.new(parent_group_id: group.id,
                          name: params[:group][:name],
                          phishing: group.phishing,
                          fraud: group.fraud)
    member = Member.new(wcu(user_id: current_user.id).merge(role: 'admin', joined_at: Time.now.utc))
    new_group.members << member
    if new_group.save!
      flash[:notice] = "Group was added successfully"
      redirect_to admin_group_path(new_group)
    end
  end

  def update_group_system_rules
    @group = current_user.admin_groups.find(params[:id])

    current_rules_ids = @group.system_rules.pluck(:source_rule_id)
    new_rules_ids = []
    if params[:sysrules].present?
      new_rules_ids = params[:sysrules].keys.map(&:to_i)
      on_rules = new_rules_ids - current_rules_ids
      Rule.find(on_rules).each do |rule|
        options = rule.slice(:name, :message, :description, :patterns, :rule_type, :quarantine,
                             :send_author, :send_admins, :include_post_content, :include_restore_link,
                             :phishing, :send_phishing_admins, :fraud, :send_fraud_admins)
        @group.system_rules << Rule.create(options.merge group_id: @group.id, source_rule_id: rule.id)
      end
    end
    off_rules = current_rules_ids - new_rules_ids
    if off_rules.present?
      @group.system_rules.where(source_rule_id: off_rules).soft_delete
    end
    redirect_to rules_admin_group_path(@group)
  end

  def create
    group = Group.new wcu(group_params)
    member = Member.new(wcu(group_admin_params).merge(role: "admin"))
    group.members << member
    if group.save
      UserMailer.invite_user_to_group(member, group).deliver_later
      flash[:notice] = "Business account was added successfully"
      redirect_to "/admin/groups"
    else
      @top_level_groups = @system_rules =  @business_rules =[]
      render :index
    end
  end

  def update
    group = Group.top_level.find(params[:id])
    group.attributes = group_params
    if group.save
      flash[:notice] = "Business account was updated successfully"
      redirect_to "/admin/groups"
    else
      @top_level_groups = @system_rules =  @business_rules =[]
      render :index
    end
  end

  def options
    @group = current_user.admin_groups.find(params[:id])
  end

  def save_options
    @group = current_user.admin_groups.find(params[:id])
    @group.attributes = group_params
    if @group.save
      flash[:notice] = "Branding was updated successfully"
      redirect_to admin_group_path(@group)
    else
      render :options
    end
  end

  private

  def group_params
    params.require(:group).permit(:name, :alert_emails, :alert_phones, :max_users, :domain,
                                  :phishing, :phishing_alert_emails, :phishing_alert_phones,
                                  :fraud, :fraud_alert_emails, :fraud_alert_phones,
                                  :title, :description, :header, :footer)
  end

  def group_admin_params
    params.require(:group_admin).permit(:invite_first_name, :invite_last_name, :invite_email)
  end

  def group_member_params
    params.require(:group_member).permit(:invite_first_name, :invite_last_name, :invite_email)
  end

  def rule_params
    params.require(:rule).permit(:name, :message, :description, :patterns, :exception, :rule_type, :quarantine,
                                 :send_author, :send_admins, :include_post_content, :include_restore_link,
                                 :phishing, :send_phishing_admins, :fraud, :send_fraud_admins)
  end

  def import_params
    params.require(:import).permit(:body, :source)
  end

  def set_admin_group
    if (@group = current_user.admin_groups.find_by(id: params[:id]))
      @role = 'admin'
      return
    end
    if (@group = current_user.phishing_admin_groups.find_by(id: params[:id]))
      @role = 'phishing_manager'
      return
    end
    if (@group = current_user.fraud_admin_groups.find_by(id: params[:id]))
      @role = 'fraud_manager'
      return
    end
    raise "invalid request"
  end

end
