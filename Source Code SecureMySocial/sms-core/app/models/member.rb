class Member < ActiveRecord::Base
  before_create :generate_token
  belongs_to :group
  belongs_to :user

  scope :pending, -> { where(joined_at: nil) }

  def self.join(user, token)
    member = where(token: token).first
    raise unless member

    transaction do
      member.user_id = user.id
      member.joined_at = Time.now.utc
      user.email = user.email.presence || member.invite_email
      user.first_name = user.first_name.presence || member.invite_first_name
      user.last_name = user.last_name.presence || member.invite_last_name
      member.save!
      user.save!
    end

    member
  end

  def full_name
    "#{invite_first_name} #{invite_last_name}"
  end

  protected

  def generate_token
    return unless token.blank?
    self.token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless self.class.exists?(token: random_token)
    end
  end
end
