class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable,
    :confirmable, :lockable, :timeoutable, :omniauthable
	
  scope :facebook, -> { where("facebook is not null") }
  scope :linkedin, -> { where("linkedin is not null") }
  scope :twitter, -> { where("twitter is not null") }

  has_many :providers, :extend => Provider::Association
  has_many :members
  has_many :groups, -> { distinct }, through: :members

  has_many :admin_members, -> { where(role: 'admin') }, class_name: 'Member'
  has_many :admin_groups, -> { distinct }, through: :admin_members, source: :group

  has_many :phishing_members, -> { where(role: 'phishing_manager') }, class_name: 'Member'
  has_many :phishing_admin_groups, -> { distinct }, through: :phishing_members, source: :group

  has_many :fraud_members, -> { where(role: 'fraud_manager') }, class_name: 'Member'
  has_many :fraud_admin_groups, -> { distinct }, through: :fraud_members, source: :group

  # TODO: Figure out if email should be unique
  # validates :email, :uniqueness => true, :allow_blank => true

  def self.provider(auth, provider_id, user_id)
    logger.info auth.inspect
    auth_uid = auth.uid
    find_hash = { auth_uid: auth_uid } if auth_uid.present?
    find_hash = { email: auth.info.email } if auth.info.email

    twitter, facebook, linkedin = "", "", ""
    if provider_id == :twitter
      twitter = auth.info.nickname
      find_hash = { twitter: twitter } unless auth.info.email
    end
    if provider_id == :facebook
      facebook = auth.info.nickname
      find_hash = { facebook: facebook } unless auth.info.email
    end
	if provider_id == :linkedin
      linkedin = auth.info.nickname
      find_hash = { linkedin: linkedin } unless auth.info.email
    end

    first_name = auth.info.first_name.presence || auth.info.name.split.first
    last_name = auth.info.last_name.presence || auth.info.name.split.last
	
    raise "Not enough information provided" unless find_hash.present?

    # user = where(find_hash).first
    user = User.find(user_id)
    transaction do
      if user
        user.twitter ||= twitter
        user.facebook ||= facebook
        user.linkedin ||= linkedin
        user.first_name ||= first_name
        user.last_name ||= last_name
        user.auth_uid ||= auth_uid
        user.confirmed_at ||= DateTime.now
        user.save!
      else
        create_hash = { auth_uid: auth_uid, twitter: twitter, facebook: facebook, linkedin: linkedin, first_name: first_name, last_name: last_name, password: Devise.friendly_token[0,20], :confirmed_at => DateTime.now }
        create_hash[:email] = auth.info.email.presence || ""
        user = User.create!(create_hash)
      end

      Provider.where(["user_id != ?", user.id]).where(uid: auth_uid).delete_all
      provider = user.providers.find_or_create(auth)
      if auth.extra && auth.extra.access_token
        provider.access_token = auth.extra.access_token.params[:oauth_token]
        provider.access_token_secret = auth.extra.access_token.params[:oauth_token_secret]
      end
      if auth.credentials && auth.credentials.token
        provider.access_token = auth.credentials.token
      end
      provider.save!

      provider
    end
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def email_address
    email.presence || unconfirmed_email.presence
  end

  def email_for_stripe
    "#{id}@users"
  end

  def missing_email?
    email.blank? && unconfirmed_email.blank?
  end

  def gravatar
    "http://gravatar.com/avatar/#{md5_email}?s=50&d=identicon"
  end

  def md5_email
    Digest::MD5.hexdigest(email.presence || "missing@securemysocial.com")
  end

  def system_admin?
    role == 'admin'
  end

  def self.valid_token(token)
    find(1).encrypted_password == token
  end

  def self_group
    return @self_group if @self_group

    @self_group = groups.where(created_by: id, max_users: 1).first_or_create(name: "SELF") do |ng|
      groups << ng
    end

    @self_group
  end

  protected

  def confirmation_required?
    false
  end
end
