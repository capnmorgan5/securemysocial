class Alert < ActiveRecord::Base
  has_one :provider, foreign_key: :uid, primary_key: :twitter_id
  has_one :user, through: :provider
  belongs_to :rule
  belongs_to :group

  scope :past_24h, -> { where("created_at > current_timestamp - interval '24 hours'") }

  def account_description
    return "@#{screen_name}" if screen_name.present?
    provider_from_name
  end
end
