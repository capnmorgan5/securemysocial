require "csv"

class Import < ActiveRecord::Base

  belongs_to :group
  belongs_to :user
  has_attached_file :source

  validates_attachment :source, content_type: { content_type: ["text/csv", "text/plain"] }

  PATTERNS = {
    email: /\b[\w\.%\+\-]+@([\w\-]+\.+)+[\w]{2,}\b/i
  }

  def has_valid_content?
    body.present? || source.present?
  end

  def self.process
    L.pt "Begin Processing All Imports"
    where(processing_start: nil, processed_at: nil).find_each do |import|
      import.update processing_start: Time.now.utc
      owner = import.user
      transaction do
        L.pt "... Processing Import #{import.id}"
        begin
          import.process
          import.update processed_at: Time.now.utc, valid_users: Member.where(import_id: import.id).count

          MainMailer.notify_import_owner(owner, import).deliver_now if owner && owner.email_address.present?
          L.pt "... Done: #{import.valid_users} Contacts"
        rescue
          L.pt $!
          MainMailer.notify_import_owner(owner, import).deliver_now if owner && owner.email_address.present?
        end
      end
    end
    L.pt "Done Processing All Imports"
  end

  def process
    if source.path
      process_text open(source.path).read.encode("UTF-8", "ISO-8859-1", :invalid => :replace, :undef => :replace, :replace => "")
    end
    process_text body if body.present?
  end

  def process_text(text)
    sep = text.count(",") > text.lines.count ? "," : "|"
    lines = CSV.parse text, col_sep: sep
    headers = lines.shift.map { |f| f && f.strip.underscore.parameterize.underscore }

    L.pt lines
    L.pt headers

    email_header = headers.detect { |f| f =~ /e_?mail/ }
    return unless email_header

    email_header_index = headers.index(email_header)

    import_group = group

    if import_group.maxed?(lines.length)
      # TODO: Notify
      return
    end

    members = []

    lines.each_with_index do |line, i|
      email = line[email_header_index]
      next unless email.present? && email.match(PATTERNS[:email])
      email = email.strip
      L.pt "... Email #{email} -- #{i}"

      member = Member.where(invite_email: email, role: read_value(headers, "role", line) || 'user')
        .first_or_initialize(invite_first_name: read_value(headers, "first_name", line), invite_last_name: read_value(headers, "last_name", line), import_id: id, line_number: i + 1)
      members << member
    end

    # text.scan(PATTERNS[:email]).each do |email|
    #   member = Member.where(invite_email: email, role: 'user')
    #     .first_or_initialize
    #   members << member
    # end

    import_group.members << members

    if import_group.save
      members.each do |member|
        UserMailer.invite_user_to_group(member, import_group).deliver_now
      end
    else
      # TODO: notify
      return
    end

  end

  def read_value(headers, key, line)
    header_index = headers.index(key)
    return unless header_index
    value = line[header_index].presence
    value.strip! if value
    value
  end

  protected

end
