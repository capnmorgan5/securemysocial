class Group < ActiveRecord::Base

  has_many :members
  has_many :users, -> { distinct }, through: :members
  has_many :admin_members, -> { where(role: 'admin') }, class_name: 'Member'
  has_many :admin_users, -> { distinct }, through: :admin_members, source: :user
  has_many :regular_members, -> { where(role: 'user') }, class_name: 'Member'
  has_many :regular_users, -> { distinct }, through: :regular_members, source: :user
  has_many :fraud_members, -> { where(role: 'fraud_manager') }, class_name: 'Member'
  has_many :fraud_managers, -> { distinct }, through: :fraud_members, source: :user
  has_many :phishing_members, -> { where(role: 'phishing_manager') }, class_name: 'Member'
  has_many :phishing_managers, -> { distinct }, through: :phishing_members, source: :user

  has_many :rules
  has_many :system_rules, -> { where("deleted_at is null and rule_type in ('system', 'personal', 'career', 'financial') and source_rule_id is not null") }, class_name: 'Rule'

  belongs_to :parent, foreign_key: 'parent_group_id', class_name: 'Group'
  has_many :sub_groups, foreign_key: 'parent_group_id', class_name: 'Group'
  has_many :imports

  validates :name, presence: true

  scope :top_level, -> { where(parent_group_id: nil) }
  scope :with_counts, -> { select("groups.*,
                                   (select count(id) from members where group_id = groups.id and role = 'admin') as admins_count,
                                   (select count(id) from members where group_id = groups.id and role = 'user') as users_count") }

  has_many :alerts

  def top_level?
    parent_group_id.nil?
  end

  def description
    (top_level? ? "Account: " : "Group: ") + name
  end

  def maxed?(to_add=1)
    max = top_level? ? max_users : parent.max_users
    return false unless max
    gid = top_level? ? id : parent.id
    group_ids = Group.where(["id = :gid or parent_group_id = :gid", gid: gid]).pluck(:id)
    Member.where(group_id: group_ids).count + to_add > max
  end

end
