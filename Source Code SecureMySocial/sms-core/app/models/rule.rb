class Rule < ActiveRecord::Base
  belongs_to :group

  scope :active, -> { where(deleted_at: nil) }

  scope :global, -> { active.where(group_id: nil).order(:name) }
  scope :non_system, -> { active.where(source_rule_id: nil).order(:name) }
  scope :regular, -> { active.where(exception: false).order(:name) }
  scope :exception, -> { active.where(exception: true).order(:name) }

  scope :system, -> { regular.where(rule_type: 'system').order(:name) }
  scope :business, -> { regular.where(rule_type: 'business').order(:name) }
  scope :custom, -> { regular.where(rule_type: 'custom').order(:name) }
  scope :personal, -> { regular.where(rule_type: 'personal').order(:name) }
  scope :career, -> { regular.where(rule_type: 'career').order(:name) }
  scope :financial, -> { regular.where(rule_type: 'financial').order(:name) }

  def self.with_selection_for_group(group_id)
    select("rules.*, sr.id as sr_rule_id, sr.message as sr_message, sr.quarantine as sr_quarantine, sr.include_restore_link as sr_include_restore_link,
            sr.send_author as sr_send_author, sr.send_admins as sr_send_admins, sr.include_post_content as sr_include_post_content,
            sr.phishing as sr_phishing, sr.send_phishing_admins as sr_send_phishing_admins,
            sr.fraud as sr_fraud, sr.send_fraud_admins as sr_send_fraud_admins")
      .joins("left outer join rules sr on sr.deleted_at is null and sr.group_id = #{group_id} and sr.source_rule_id = rules.id")

  end

  def self.soft_delete
    update_all(deleted_at: Time.now.utc)
  end

  def patterns_count
    patterns.lines.length
  end

  def normalized_patterns
    patterns.lines.map { |line| line.strip }
  end

  def anchor
    exception ? "ignore" : rule_type
  end
end
