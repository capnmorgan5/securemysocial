module ApplicationHelper

  def by_role(collection, role)
    collection.where(role: role)
  end
end
