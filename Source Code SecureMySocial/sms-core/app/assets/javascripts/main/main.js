$(function() {

  $("input.ajax-save").on("click", function() {
    this.form.submit();
  });

  $(".edit-group-icon").click(function() {
    var $this = $(this);
    $("#edit-group-form").attr("action", "/admin/groups/" + $this.data("group-id"));
    $("#group-name").val($this.data("group-name"));
    $("#group-max-users").val($this.data("max-users"));
    $("#group-domain").val($this.data("domain"));
    $("#group-phishing").prop("checked", $this.data("phishing"));
    $("#group-fraud").prop("checked", $this.data("fraud"));

    $("#edit-group-modal").modal();
    return false;
  });

  $(".edit-rule-icon").click(function() {
    var $this = $(this);
    $("#rule-id").val($this.data("rule-id"));
    $("#rule-name").val($this.data("rule-name"));
    $("#rule-message").val($this.data("rule-message"));
    $("#rule-description").val($this.data("rule-description"));
    $("#rule-patterns").val($this.data("rule-patterns"));
    $("#rule-quarantine").prop({ checked: $this.data("rule-quarantine") });
    $("#rule-send-author").prop({ checked: $this.data("rule-send-author") });
    $("#rule-send-admins").prop({ checked: $this.data("rule-send-admins") });
    $("#rule-phishing").prop({ checked: $this.data("rule-phishing") });
    $("#rule-send-phishing-admins").prop({ checked: $this.data("rule-send-phishing-admins") });
    $("#rule-fraud").prop({ checked: $this.data("rule-fraud") });
    $("#rule-send-fraud-admins").prop({ checked: $this.data("rule-send-fraud-admins") });
    $("#rule-include-post-content").prop({ checked: $this.data("rule-include-post-content") });
    $("#rule-include-restore-link").prop({ checked: $this.data("rule-include-restore-link") });

    $("#edit-rule-modal").modal();
    return false;
  });

  $(".edit-system-rule-icon").click(function() {
    var $this = $(this);
    $("#system-rule-id").val($this.data("rule-id"));
    $("#system-rule-message").val($this.data("rule-message"));
    $("#system-rule-quarantine").prop({ checked: $this.data("rule-quarantine") });
    $("#system-rule-send-author").prop({ checked: $this.data("rule-send-author") });
    $("#system-rule-send-admins").prop({ checked: $this.data("rule-send-admins") });
    $("#system-rule-phishing").prop({ checked: $this.data("rule-phishing") });
    $("#system-rule-send-phishing-admins").prop({ checked: $this.data("rule-send-phishing-admins") });
    $("#system-rule-fraud").prop({ checked: $this.data("rule-fraud") });
    $("#system-rule-send-fraud-admins").prop({ checked: $this.data("rule-send-fraud-admins") });
    $("#system-rule-include-post-content").prop({ checked: $this.data("rule-include-post-content") });
    $("#system-rule-include-restore-link").prop({ checked: $this.data("rule-include-restore-link") });

    $("#edit-system-rule-modal").modal();
    return false;
  });

  $(".edit-business-rule-icon").click(function() {
    var $this = $(this);
    $("#bs-rule-id").val($this.data("rule-id"));
    $("#bs-rule-name").val($this.data("rule-name"));
    $("#bs-rule-description").val($this.data("rule-description"));

    $("#edit-business-rule-modal").modal();
    return false;
  });

  $("select#group_id").change(function() {
    document.location = "/admin/groups/" + this.value;
  });

  var hash = window.location.hash;
  if (hash) {
    $('ul.nav a[href="' + hash + '"]').tab('show');
  } else {
    $('ul.nav a:first').tab('show');
  }

  $('.nav-tabs a').click(function (e) {
    $(this).tab('show');
    var scrollmem = $('body').scrollTop();
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);
  });

  $("#dashboard .count").each(function(i, countElem) {
    var $countElem = $(countElem);
    $({ someValue: 0 }).animate({ someValue: +$countElem.text() }, {
      duration: 750,
      easing: 'swing',
      step: function () {
        $countElem.text(commaSeparateNumber(Math.round(this.someValue)));
      }
    });
  });

  function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
      val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }
    return val;
  }

});
