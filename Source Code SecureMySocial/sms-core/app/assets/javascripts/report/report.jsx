$(function() {
  "use strict";

  let Column = FixedDataTable.Column;
  let PropTypes = React.PropTypes;
  let Table = FixedDataTable.Table;
  let isColumnResizing;

  let Report = React.createClass({
    propTypes: {
      onContentDimensionsChange: PropTypes.func,
      left: PropTypes.number,
      top: PropTypes.number,
    },

    getInitialState() {
      return {
        rows: [],
        filteredRows: [],
        columnWidths: {
          accountInfo: 200,
          accountEmail: 100,
          accountPhone: 100,
          postWrittenAt: 95,
          ruleName: 100,
          postDeletedAt: 95,
          phishingRelated: 75,
          fraudRelated: 75,
          postMessage: 300
        }
      }
    },

    componentDidMount() {
      $.ajax(`/admin/groups/${currentGroupId}/alerts`)
        .success( data => this.setState( { rows: data, filteredRows: data } ) )
        .error( err => console.log && console.log(err) )
    },

    _rowGetter(index) {
      return this.state.filteredRows[index];
    },

    _onColumnResizeEndCallback(newColumnWidth, dataKey) {
      let columnWidths = this.state.columnWidths;
      columnWidths[dataKey] = newColumnWidth;
      isColumnResizing = false;
      this.setState({columnWidths: columnWidths});
    },

  _onFilterChange(event) {
    let filterBy = event.target.value;
    var rows = this.state.rows.slice();
    var filteredRows = rows.filter(function(row) {
      return [row['accountInfo'], row['accountEmail'], row['accountPhone'], row['ruleName'], row['postMessage']].join(" ").toLowerCase().indexOf(filterBy.toLowerCase()) >= 0
    })

    this.setState({
      filteredRows
    })
  },

    render() {
      let controlledScrolling = this.props.left !== undefined || this.props.top !== undefined;
      let columnWidths = this.state.columnWidths;

      return (
        <div>
          <div className="row">
            <div className="col-sm-offset-7 col-sm-5">
              <input onChange={this._onFilterChange} placeholder='Filter...' className="form-control" />
            </div>
          </div>
          <br />
          <Table
            rowHeight={30}
            headerHeight={50}
            rowGetter={this._rowGetter}
            rowsCount={this.state.filteredRows.length}
            width={this.props.tableWidth}
            maxHeight={this.props.tableHeight}
            overflowX={controlledScrolling ? "hidden" : "auto"}
            overflowY={controlledScrolling ? "hidden" : "auto"}
            isColumnResizing={isColumnResizing}
            onColumnResizeEndCallback={this._onColumnResizeEndCallback}>
            <Column
              label="Account"
              dataKey="accountInfo"
              width={columnWidths['accountInfo']}
              isResizable={true}
            />
            <Column
              label="Email"
              dataKey="accountEmail"
              width={columnWidths['accountEmail']}
              isResizable={true}
            />
            <Column
              label="Phone"
              dataKey="accountPhone"
              width={columnWidths['accountPhone']}
              isResizable={true}
            />
            <Column
              label="Written At"
              dataKey="postWrittenAt"
              width={columnWidths['postWrittenAt']}
              isResizable={true}
            />
            <Column
              label="Deleted At"
              dataKey="postDeletedAt"
              width={columnWidths['postDeletedAt']}
              isResizable={true}
            />
            <Column
              label="Phishing Related"
              dataKey="phishingRelated"
              width={columnWidths['phishingRelated']}
              isResizable={true}
            />
            <Column
              label="Fraud Related"
              dataKey="fraudRelated"
              width={columnWidths['fraudRelated']}
              isResizable={true}
            />
            <Column
              label="Rule"
              dataKey="ruleName"
              width={columnWidths['ruleName']}
              isResizable={true}
            />
            <Column
              label="Post"
              dataKey="postMessage"
              width={columnWidths['postMessage']}
              flexGrow={2}
              isResizable={true}
            />
          </Table>
        </div>
      );
    }
  });

  var Main = React.createClass({
    getInitialState() {
      return {
        renderPage: false
      };
    },

    render() {
      return (
        <div>
          {this.state.renderPage && this._renderPage()}
        </div>
      );
    },

    _renderPage() {
      return (
        <Report {...this.state} />
      );
    },

    componentDidMount() {
      this._update();
      var win = window;
      if (win.addEventListener) {
        win.addEventListener('resize', this._onResize, false);
      } else if (win.attachEvent) {
        win.attachEvent('onresize', this._onResize);
      } else {
        win.onresize = this._onResize;
      }
    },

    _onResize() {
      clearTimeout(this._updateTimer);
      this._updateTimer = setTimeout(this._update, 16);
    },

    _update() {
      var win = window;

      var widthOffset = win.innerWidth < 680 ? 0 : 50;

      this.setState({
        renderPage: true,
        tableWidth: win.innerWidth - widthOffset,
        tableHeight: win.innerHeight - 375,
      });
    }
  });

  React.render(
    <Main />,
    document.getElementById('report')
  );
});
