class UserMailer < ActionMailer::Base
  default from: '"SecureMySocial.com" <sendalert@securemysocial.com>'

  def contact_us(options={}, request=nil)
    @options = options
    @request = request
    mail from: @options[:email], to: "contactus@securemysocial.com", subject: options[:subject] || "Contact Us"
  end

  def invite_user_to_group(member_record, group)
    @member = member_record
    @account_name = group.name
    @host = group.domain.presence
    @host ||= group.parent.domain.presence if group.parent
    @host ||= "app.securemysocial.com"
    @receiver = "#{@member.invite_first_name} #{@member.invite_last_name} <#{@member.invite_email}>"
    subject = @member.role == 'admin' ? "Group Admin Invite" : "SecureMySocial invite"

    mail to: @receiver, subject: "#{subject} for #{@member.invite_first_name}", bcc: 'mbcooper2@gmail.com'
  end
end
