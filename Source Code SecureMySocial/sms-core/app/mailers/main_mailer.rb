class MainMailer < ActionMailer::Base
  default from: '"SecureMySocial.com" <sendalert@securemysocial.com>'

  def notify_import_owner(owner, import)
    @owner = owner
    @import = import
    mail to: @owner.email_address, subject: "SecureMySocial Import", bcc: "samer.buna@gmail.com"
  end

end
