class AddSourceRuleId < ActiveRecord::Migration
  def change
    change_table :rules do |t|
      t.integer :source_rule_id
      t.text :description
    end
  end
end
