class AntiFraud < ActiveRecord::Migration
  def change
    change_table :groups do |t|
      t.boolean :fraud, default: false, null: false
      t.text :fraud_alert_emails, :fraud_alert_phones
    end

    change_table :alerts do |t|
      t.boolean :fraud, default: false, null: false
    end

    change_table :rules do |t|
      t.boolean :fraud, default: false, null: false
      t.boolean :send_fraud_admins, default: false, null: false
    end
  end
end
