class AddBranding < ActiveRecord::Migration
  def change
    change_table :groups do |t|
      t.string :domain, :title, :description
      t.text :header, :footer
    end
  end
end
