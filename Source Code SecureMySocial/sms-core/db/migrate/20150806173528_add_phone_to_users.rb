class AddPhoneToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :phone
      t.boolean :alert_email, default: true, null: false
      t.boolean :alert_phone, default: false, null: false
    end
  end
end
