class AddFacebookToAlerts < ActiveRecord::Migration
  def change
    change_table :alerts do |t|
      t.rename :tweet_text, :message
      t.timestamp :message_time
      t.string :provider_uid
      t.string :provider_from_name
      t.references :post
    end
  end
end
