class AuthorizeAutoDelete < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.boolean :auto_delete, default: true, null: false
    end
  end
end
