class AddProcessedAtToPosts < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.timestamp :processed_at
    end
  end
end
