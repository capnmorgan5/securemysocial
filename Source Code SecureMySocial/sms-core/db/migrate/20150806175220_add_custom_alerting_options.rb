class AddCustomAlertingOptions < ActiveRecord::Migration
  def change
    change_table :rules do |t|
      t.boolean :send_author, default: true, null: false
      t.boolean :send_admins, default: false, null: false
    end
    change_table :groups do |t|
      t.text :alert_emails
    end
  end
end
