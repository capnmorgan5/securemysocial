class CreateProvidersTable < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.references :user, :null => false
      t.string :provider, :uid, :null => false

      t.timestamps
    end
  end
end
