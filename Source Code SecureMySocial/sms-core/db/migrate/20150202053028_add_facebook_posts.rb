class AddFacebookPosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :user
      t.string :provider
      t.string :provider_uid
      t.string :provider_from_uid
      t.string :provider_from_name
      t.text :message
      t.timestamp :message_time

      t.timestamps
    end
  end
end
