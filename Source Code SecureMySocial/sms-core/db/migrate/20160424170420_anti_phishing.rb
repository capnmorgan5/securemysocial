class AntiPhishing < ActiveRecord::Migration
  def change
    change_table :groups do |t|
      t.boolean :phishing, default: false, null: false
      t.text :phishing_alert_emails, :phishing_alert_phones
    end

    change_table :alerts do |t|
      t.boolean :phishing, default: false, null: false
    end

    change_table :rules do |t|
      t.boolean :phishing, default: false, null: false
      t.boolean :send_phishing_admins, default: false, null: false
    end
  end
end
