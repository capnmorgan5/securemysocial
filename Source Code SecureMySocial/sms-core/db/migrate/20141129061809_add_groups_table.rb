class AddGroupsTable < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name, null: false
      t.integer :parent_group_id

      t.integer :created_by
      t.timestamps
    end

    create_table :members do |t|
      t.integer :group_id
      t.integer :user_id
      t.string :role

      t.string :invite_first_name, :invite_last_name, :invite_email

      t.string :token

      t.integer :created_by
      t.timestamps
    end
  end
end
