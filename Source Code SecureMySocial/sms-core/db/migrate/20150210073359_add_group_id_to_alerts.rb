class AddGroupIdToAlerts < ActiveRecord::Migration
  def change
    change_table :alerts do |t|
      t.references :group
    end

    add_foreign_key :alerts, :groups
  end
end
