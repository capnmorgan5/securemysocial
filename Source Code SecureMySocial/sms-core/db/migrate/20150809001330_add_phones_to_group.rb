class AddPhonesToGroup < ActiveRecord::Migration
  def change
    change_table :groups do |t|
      t.text :alert_phones
    end
  end
end
