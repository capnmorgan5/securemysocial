process.stdin.resume();

require("console-stamp")(console, "HH:MM:ss.l");

var Twit = require('twit');

var T = new Twit({
  consumer_key: process.argv[2],
  consumer_secret: process.argv[3],
  access_token: process.argv[4],
  access_token_secret: process.argv[5]
});

var MongoClient = require('mongodb').MongoClient, assert = require('assert');

MongoClient.connect(process.env.MONGOLAB_URI, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to mongodb server");
  var collection = db.collection('tweets');
  var stream = T.stream('user', { with: 'user' });

  stream.on('tweet', function (tweet) {
    console.log(tweet.text);
    collection.insert(tweet, function(err, result) {
      assert.equal(err, null);
      console.log("Inserted Tweet");
    });
  });

  stream.on('limit', function (limitMessage) {
    console.log("limit", limitMessage);
  });

  stream.on('disconnect', function (disconnectMessage) {
    console.log("disconnect", disconnectMessage);
  });

  stream.on('connect', function (request) {
    console.log("connect");
  });

  // stream.on('connected', function (response) {
  //   console.log("connected");
  // });

  stream.on('reconnect', function (request, response, connectInterval) {
    console.log("reconnect", connectInterval);
  });

  stream.on('warning', function (warning) {
    console.log("warning, warning");
  });

  // stream.on('user_event', function (eventMsg) {
  //   console.log("user_event", eventMsg);
  // })

  function exitHandler(options, err) {
    stream.stop();
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
    if (options.exit) process.exit();
  }

  //do something when app is closing
  process.on('exit', exitHandler.bind(null,{cleanup:true}));

  //catches ctrl+c event
  process.on('SIGINT', exitHandler.bind(null, {exit:true}));

  //catches uncaught exceptions
  process.on('uncaughtException', exitHandler.bind(null, {exit:true}));;
});
