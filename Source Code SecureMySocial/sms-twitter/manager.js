var mps = require('child_process');

var pg = require('pg');
var monitors = [];

var readUsers = function() {
  pg.connect(process.env.PG_CONN_STRING, function(err, client, done) {
    if(err) { return console.error('error fetching client from pool', err); }

    console.log("Reading Users...");
    var currentIds = [0];
    if (monitors.length > 0) { currentIds = Object.keys(monitors); }

    client.query("select user_id, uid, access_token, access_token_secret from providers where provider = 'twitter' and user_id not in (" + currentIds.join(",") + ")", [], function(err, result) {
      done();

      if(err) { return console.error('error running query', err); }

      result.rows.forEach(function(up) {
        console.log("ion " + up.user_id);
        monitors[up.user_id] = mps.spawn('node', ['index.js', 'wK2VcMoYpftHJTWeiNZTYYSpK', 'gWhGb0mkZqRV7B68DzpC0S0OYN3CRrTicf7iaM5pspgicXxQt7', up.access_token, up.access_token_secret]);

        monitors[up.user_id].stdout.on('data', function (data) {
          console.log(up.user_id + '|stdout: ' + data);
        });

        monitors[up.user_id].stderr.on('data', function (data) {
          console.log(up.user_id + '|stderr: ' + data);
        });

        monitors[up.user_id].on('close', function (code) {
          console.log(up.user_id + '|child process exited with code ' + code);
        });
      });

      setTimeout(readUsers, 60000);
    });
  });
};

readUsers();
