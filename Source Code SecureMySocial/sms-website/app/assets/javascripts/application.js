//= require_self
//= require_tree .

$("#contact-form").submit(function(){
  var email = $("#contact-email").val(); var name = $("#contact-name").val(); var msg = $("#contact-message").val();
  $.ajax({ type: "POST", url: "https://pmail.herokuapp.com/send", data: { 'token': 'pubkey-ee1fe632afb4e6e8f66774d902797972', 'from_email': email, 'subject': 'SecureMySocial Contact Form', 'body': msg + "\n\n" + name + "\n" + email, 'to_email': 'info@securemysocial.com', } })
  .done(function(response) {
    alert('Your message has been sent. Thank you!'); $("#contact-name,#contact-email,#contact-message").val('');
  })
  .fail(function(response) { alert('Error sending message.'); });
  return false;
});
