$(function() {
  $(".edit-group-icon").click(function() {
    var $this = $(this);
    $("#edit-group-form").attr("action", "/admin/groups/" + $this.data("group-id"));
    $("#group-name").val($this.data("group-name"));
    $("#group-max-users").val($this.data("max-users"));
    $("#group-domain").val($this.data("domain"));

    $("#edit-group-modal").modal();
    return false;
  });

  $(".edit-rule-icon").click(function() {
    var $this = $(this);
    $("#rule-id").val($this.data("rule-id"));
    $("#rule-name").val($this.data("rule-name"));
    $("#rule-message").val($this.data("rule-message"));
    $("#rule-description").val($this.data("rule-description"));
    $("#rule-patterns").val($this.data("rule-patterns"));
    $("#rule-quarantine").prop({ checked: $this.data("rule-quarantine") });

    $("#edit-rule-modal").modal();
    return false;
  });

  $(".edit-business-rule-icon").click(function() {
    var $this = $(this);
    $("#bs-rule-id").val($this.data("rule-id"));
    $("#bs-rule-name").val($this.data("rule-name"));
    $("#bs-rule-description").val($this.data("rule-description"));

    $("#edit-business-rule-modal").modal();
    return false;
  });

  $("select#group_id").change(function() {
    document.location = "/admin/groups/" + this.value;
  });
});
