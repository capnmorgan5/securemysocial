class UserMailer < ActionMailer::Base
  default from: '"SecureMySocial.com" <sendalert@securemysocial.com>'

  def contact_us(options={}, request=nil)
    @options = options
    @request = request
    mail from: @options[:email], to: "contactus@securemysocial.com", subject: options[:subject] || "Contact Us"
  end

end
