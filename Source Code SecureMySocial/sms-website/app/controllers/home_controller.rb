require 'rss'
class HomeController < ApplicationController

  before_action :set_posts

  caches_action :index, expires: 2.hours

  def set_posts
    @posts = RSS::Parser.parse(open('http://securemysocial.blogspot.com/feeds/posts/default?alt=rss').read, false).items[0..3] rescue nil
  end

end
