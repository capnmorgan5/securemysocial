Rails.application.routes.draw do
  get "/about" => "home#about"
  get "/overview" => "home#overview"
  get "/faq" => "home#faq"
  get "/contact" => "home#contact"
  get "/partners" => "home#partners"
  get "/features" => "home#features"
  get "/businesses" => "home#businesses"
  get "/parents" => "home#parents"
  get "/individuals" => "home#individuals"
  get "/blog" => "home#blog"
  get "/blog/post" => "home#blog_post"

  root to: 'home#index'
end
