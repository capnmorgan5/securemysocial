require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)

require 'clockwork'

include Clockwork

app_client = Twitter::REST::Client.new do |config|
  config.consumer_key = "wK2VcMoYpftHJTWeiNZTYYSpK"
  config.consumer_secret = "gWhGb0mkZqRV7B68DzpC0S0OYN3CRrTicf7iaM5pspgicXxQt7"
  config.access_token = "2940434510-VlnykdWwJIsMSZ03bdEfXRH3E1Kk7zZZAf39xyc"
  config.access_token_secret = "Kc7Vz6osnU1V0EgtZYfidWeQSRcfT74GRnKKAtQ7DaY5Q"
end

users = {}

def self.log message
  puts "*** #{Time.now} - #{message}"
end

def process(app_client, users)
  log "Begin Process"
  output = { tweets: [], alerts: [], posts: [] }

  exceptions = Rule.exception.all.group_by { |rule| rule.group_id }
  rules = Rule.regular.all.group_by { |rule| rule.group_id }

  Post.all.each do |post|
    output[:posts] << post.id
    provider_uid = post.provider_uid
    users[provider_uid] ||= User.where(id: post.user_id).joins(:groups).select("users.email, users.unconfirmed_email, groups.id as group_id, users.id as user_id").first

    if users[provider_uid].present?
      u = users[provider_uid]

      exception_rule = Scanner.match(post.message, exceptions[u.group_id])
      next if exception_rule

      alert_rule = Scanner.match(post.message, rules[u.group_id])
      if alert_rule
        alert_message = alert_rule.message
        email_to = u.email.presence || u.unconfirmed_email.presence
        Alert.create! message: post.message, rule_id: alert_rule.id, email_to: email_to, message_time: post.message_time,
          provider_uid: post.provider_uid, provider_from_name: post.provider_from_name, post_id: post.id, group_id: u.group_id, user_id: u.user_id
        output[:alerts] << "#{alert_message} | #{post.provider_from_name}: #{post.message}"
        UserMailer.alert(email_to, "FB: " + alert_message, post.message, false).deliver_later if email_to
      end
    end

    post.destroy
  end

  Tweet.each do |tweet|
    log tweet.inspect
    next unless tweet["user"] && tweet["text"]
    output[:tweets] << tweet["id"]
    twitter_id = tweet["user"]["id_str"]

    users[twitter_id] ||= Provider.where(uid: twitter_id).joins(:user => :groups).select("providers.*, users.email, users.unconfirmed_email, groups.id as group_id")

    if users[twitter_id].present?
      users[twitter_id].each do |provider|
        alert_rule = Scanner.match(tweet["text"], rules[provider.group_id])
        if alert_rule
          alert_message = alert_rule.message
          message = alert_message + " [#{tweet["text"].truncate(130 - alert_message.length)}]"
          email_to = provider.email.presence || provider.unconfirmed_email.presence
          Alert.create! twitter_id: twitter_id, tweet_id: tweet["id"], message: tweet["text"], rule_id: alert_rule.id, user_id: provider.user_id,
            screen_name: tweet['user']['screen_name'], email_to: email_to, group_id: provider.group_id, message_time: tweet["created_at"]
          output[:alerts] << "#{alert_message} | #{tweet['user']['screen_name']}: #{tweet['text']}"
          deleted = false
          begin
            if alert_rule.quarantine && provider.twitter_client.destroy_status(tweet["id_str"])
              puts "************** TWEET DELETED: #{tweet["id_str"]} | #{tweet["text"]}"
              deleted = true
            end
          rescue => e
            puts "**** ERROR DELETING: #{e}"
          end
          begin
            provider.twitter_client.follow("SecureSocialApp")
            app_client.create_direct_message(tweet["user"]["screen_name"], message)
          rescue => e
            puts "**** ERROR DM: #{e}"
          end
          UserMailer.alert(email_to, "Twitter: " + alert_message, tweet["text"], deleted).deliver_later if email_to
        end
      end
    end

    tweet.delete
  end

  log "End Process"
  output
end

every(30.seconds, 'Queueing interval job') do
  process(app_client, users)
end
