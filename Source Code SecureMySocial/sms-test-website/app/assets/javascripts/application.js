//= require_self
//= require_tree .

$("#contact-form").submit(function() {
  var email = $("#contact-email").val(); var name = $("#contact-name").val(); var msg = $("#contact-message").val();
  $.ajax({ type: "POST", url: "https://mandrillapp.com/api/1.0/messages/send.json", data: { 'key': 'GsdCHWv8dPGLLdEHek_bhg', 'message': { 'from_email': email, 'from_name': name, 'headers': { 'Reply-To': email }, 'subject': 'SecureMySocial Contact Form', 'text': msg, 'to': [ { 'email': 'info@securemysocial.com', 'name': 'SecureMySocial', 'type': 'to' }] } } })
  .done(function(response) { alert('Your message has been sent. Thank you!'); $("#contact-name,#contact-email,#contact-message").val(''); })
  .fail(function(response) { alert('Error sending message.'); });
  return false;
});
