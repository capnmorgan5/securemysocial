require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)

require 'clockwork'

include Clockwork

twitter_client = Twitter::REST::Client.new do |config|
  config.consumer_key = "wK2VcMoYpftHJTWeiNZTYYSpK"
  config.consumer_secret = "gWhGb0mkZqRV7B68DzpC0S0OYN3CRrTicf7iaM5pspgicXxQt7"
  config.access_token = "2940434510-VlnykdWwJIsMSZ03bdEfXRH3E1Kk7zZZAf39xyc"
  config.access_token_secret = "Kc7Vz6osnU1V0EgtZYfidWeQSRcfT74GRnKKAtQ7DaY5Q"
end

twilio_client = Twilio::REST::Client.new(ENV["TWILIO_ACCOUNT_SID"], ENV["TWILIO_AUTH_TOKEN"])

users = {}

def self.log message
  puts "*** #{Time.now} - #{message}"
end

def process(twitter_client, twilio_client, users)
  log "Begin Process"
  updated_users_uids = User.where("updated_at > current_timestamp - interval '30 minutes'").includes(:providers).all.map { |u| u.providers.map(&:uid) }.flatten
  puts "Updated Users Uids: #{updated_users_uids}"
  updated_users_uids.each do |uuuid|
    users.delete(uuuid)
  end

  output = { tweets: [], alerts: [], posts: [] }

  exceptions = Rule.active.exception.all.group_by { |rule| rule.group_id }
  rules = Rule.active.regular.all.group_by { |rule| rule.group_id }

  exceptions_scanners = {}
  rules_scanners = {}

  Post.pending.each do |post|
    output[:posts] << post.id
    provider_uid = post.provider_uid
    users[provider_uid] ||= User.where(id: post.user_id).joins(:groups)
      .select("DISTINCT users.email, users.alert_phone, users.alert_email,
               users.phone, users.unconfirmed_email, users.auto_delete,
               groups.alert_emails as admin_emails, groups.alert_phones as admin_phones,
               groups.phishing_alert_emails, groups.phishing_alert_phones,
               groups.fraud_alert_emails, groups.fraud_alert_phones,
               groups.id as group_id, users.id as user_id")
      .order("groups.id desc")

    if post.message.present? && users[provider_uid].present?
      puts "**************************************"
      puts post.inspect

      users[provider_uid].each do |author|
        if exceptions[author.group_id]
          # puts exceptions[author.group_id].map(&:patterns).inspect
          exceptions_scanners[author.group_id] ||= Scanner.new(exceptions[author.group_id])
          exception_rule = exceptions_scanners[author.group_id].first_match(post.message)
          if exception_rule
            puts "*** Matched Exception Rule: #{exception_rule.name}"
            next
          end
        end

        if rules[author.group_id]
          # puts rules[author.group_id].map(&:patterns).inspect
          rules_scanners[author.group_id] ||= Scanner.new(rules[author.group_id])
          rule = rules_scanners[author.group_id].first_match(post.message)
          if rule
            puts "*** Matched Regular Rule: #{rule.name}"
            rule.create_alert(social_provider: :facebook, author: author, post: post, twitter_client: twitter_client, twilio_client: twilio_client) if rule
            break
          end
        end
      end
    end

    post.update(processed_at: Time.now.utc, message: post.masked_message)
  end

  Tweet.each do |tweet|
    log tweet.inspect
    next unless tweet["user"] && tweet["text"]
    output[:tweets] << tweet["_id"]
    twitter_id = tweet["user"]["id_str"]

    users[twitter_id] ||= Provider.where(uid: twitter_id).joins(:user => :groups)
      .select("DISTINCT providers.*, users.alert_phone, users.alert_email,
               users.phone, users.email, users.unconfirmed_email, users.auto_delete,
               groups.phishing_alert_emails, groups.phishing_alert_phones,
               groups.fraud_alert_emails, groups.fraud_alert_phones,
               groups.id as group_id, groups.alert_emails as admin_emails, groups.alert_phones as admin_phones")
      .order("groups.id desc")

    if users[twitter_id].present?
      users[twitter_id].each do |author|
        if exceptions[author.group_id]
          # puts exceptions[author.group_id].map(&:patterns).inspect
          exceptions_scanners[author.group_id] ||= Scanner.new(exceptions[author.group_id])
          exception_rule = exceptions_scanners[author.group_id].first_match(tweet["text"])
          if exception_rule
            puts "*** Matched Exception Rule: #{exception_rule.name}"
            next
          end
        end

        if rules[author.group_id]
          puts "Author.group_id: #{author.group_id} - AutoDelete #{author.auto_delete}"
          puts rules[author.group_id].map(&:name).inspect
          rules_scanners[author.group_id] ||= Scanner.new(rules[author.group_id])
          rule = rules_scanners[author.group_id].first_match(tweet["text"])
          if rule
            puts "*** Text #{tweet["text"]} *** Matched Regular Rule #{rule.name} :: #{rule.patterns}"
            rule.create_alert(social_provider: :twitter, author: author, post: tweet, twitter_client: twitter_client, twilio_client: twilio_client) if rule
            break
          end
        end
      end
    end

    tweet.delete
  end

  log "End Process"
  log output
end

every(30.seconds, 'Queueing interval job') do
  process(twitter_client, twilio_client, users)
end
