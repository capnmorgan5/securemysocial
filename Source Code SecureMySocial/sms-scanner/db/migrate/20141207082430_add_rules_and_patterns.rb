class AddRulesAndPatterns < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.integer :group_id
      t.string :name, null: false
      t.string :message, null: false
      t.text :patterns, null: false

      t.integer :created_by
      t.timestamp :deleted_at

      t.timestamps
    end
  end
end
