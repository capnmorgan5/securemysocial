class AddImports < ActiveRecord::Migration
  def change
    create_table :imports do |t|
      t.integer  :user_id, null: false
      t.integer  :group_id, null: false
      t.text     :body
      t.datetime :processing_start
      t.datetime :processed_at
      t.integer  :valid_users, default: 0, null: false

      t.attachment :source
      t.timestamps
    end

    change_table :members do |t|
      t.integer :import_id, :line_number
    end
  end
end
