class AddJoinedAtToMembers < ActiveRecord::Migration
  def change
    change_table :members do |t|
      t.timestamp :joined_at
    end
  end
end
