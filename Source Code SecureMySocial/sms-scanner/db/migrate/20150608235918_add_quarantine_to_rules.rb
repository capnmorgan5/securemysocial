class AddQuarantineToRules < ActiveRecord::Migration
  def change
    change_table :rules do |t|
      t.boolean :quarantine, default: false, null: false
    end
    change_table :alerts do |t|
      t.boolean :quarantine, default: false, null: false
    end
    change_table :posts do |t|
      t.boolean :quarantine, default: false, null: false
    end
  end
end
