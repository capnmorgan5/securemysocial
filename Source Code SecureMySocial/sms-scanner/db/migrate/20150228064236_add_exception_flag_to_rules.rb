class AddExceptionFlagToRules < ActiveRecord::Migration
  def change
    change_table :rules do |t|
      t.boolean :exception, default: false, null: false
    end
  end
end
