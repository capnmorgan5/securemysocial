class AddAlerts < ActiveRecord::Migration
  def change
    create_table :alerts do |t|
      t.string :twitter_id, :tweet_id, :screen_name, :email_to
      t.text :tweet_text
      t.references :rule

      t.timestamps
    end

    add_foreign_key :alerts, :rules
  end
end
