class AddConstraints < ActiveRecord::Migration
  def change
    execute "alter table rules add constraint uniq_rule_parent unique (group_id, source_rule_id, deleted_at)"
  end
end
