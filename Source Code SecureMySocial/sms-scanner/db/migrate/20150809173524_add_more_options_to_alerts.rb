class AddMoreOptionsToAlerts < ActiveRecord::Migration
  def change
    change_table :alerts do |t|
      t.rename :email_to, :author_email
      t.string :author_phone
      t.string :post_source
      t.string :post_place
      t.boolean :author_email_alerted, :author_phone_alerted, :admins_alerted
      t.timestamp :post_deleted_at
    end
  end
end
