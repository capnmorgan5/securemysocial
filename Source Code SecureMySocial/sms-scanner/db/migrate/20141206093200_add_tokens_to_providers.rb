class AddTokensToProviders < ActiveRecord::Migration
  def change
    change_table :providers do |t|
      t.string :access_token, :access_token_secret
    end
  end
end
