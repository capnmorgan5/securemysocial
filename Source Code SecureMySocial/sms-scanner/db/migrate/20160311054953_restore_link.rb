class RestoreLink < ActiveRecord::Migration
  def change
    change_table :rules do |t|
      t.boolean :include_restore_link, default: false, null: false
    end
  end
end
