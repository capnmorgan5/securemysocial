class AddRuleType < ActiveRecord::Migration
  def change
    change_table :rules do |t|
      t.string :rule_type, default: 'regular', null: false
    end

    Rule.where(group_id: nil).update_all("rule_type = 'system'")
  end
end
