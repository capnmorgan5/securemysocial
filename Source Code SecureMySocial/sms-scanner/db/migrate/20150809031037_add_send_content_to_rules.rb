class AddSendContentToRules < ActiveRecord::Migration
  def change
    change_table :rules do |t|
      t.boolean :include_post_content, default: true, null: false
    end
  end
end
