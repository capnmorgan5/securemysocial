class AddUserIdToPosts < ActiveRecord::Migration
  def change
    change_table :alerts do |t|
      t.references :user
    end

    add_foreign_key :alerts, :users
  end
end
