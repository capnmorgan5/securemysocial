class AddTweetsExceptions < ActiveRecord::Migration
  def change
    create_table :deleted_tweets do |t|
      t.integer :tweet_id, null: false
      t.text :id_str, null: false
      t.string :screen_name, null: false
      t.text :tweet, null: false
      t.timestamp :tweeted_at, null: false
      t.timestamp :deleted_at, null: false

      t.timestamps
    end
  end
end
