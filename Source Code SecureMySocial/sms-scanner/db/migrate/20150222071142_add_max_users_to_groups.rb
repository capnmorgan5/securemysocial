class AddMaxUsersToGroups < ActiveRecord::Migration
  def change
    change_table :groups do |t|
      t.integer :max_users
    end
  end
end
