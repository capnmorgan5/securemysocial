class Rule < ActiveRecord::Base
  belongs_to :group

  scope :active, -> { where(deleted_at: nil) }
  scope :system, -> { where(rule_type: 'system') }
  scope :personal, -> { where(rule_type: 'personal') }
  scope :career, -> { where(rule_type: 'career') }
  scope :financial, -> { where(rule_type: 'financial') }
  scope :business, -> { where(rule_type: 'business') }
  scope :global, -> { where(group_id: nil) }
  scope :non_system, -> { where(source_rule_id: nil) }
  scope :regular, -> { where(exception: false) }
  scope :exception, -> { where(exception: true) }

  def patterns_count
    patterns.lines.length
  end

  def normalized_patterns
    patterns.lines.map { |line| line.strip.squeeze(" ") }.reject { |c| c.blank? }
  end

  def self.soft_delete
    update_all(deleted_at: Time.now.utc)
  end

  def content(message)
    include_post_content ? message : "[...post content omitted...] "
  end

  def create_alert(social_provider:, author:, post:, twitter_client:, twilio_client:)
    alert_message = message
    author_email = author.email.presence || author.unconfirmed_email.presence
    post_deleted_at = nil

    case social_provider
    when :facebook
      raw_message = post.message
      post_message = content(post.message)
      alert_options = { message_time: post.message_time, provider_uid: post.provider_uid, provider_from_name: post.provider_from_name, post_id: post.id }
    when :twitter
      tweet = post
      raw_message = tweet["text"]
      post_message = content(tweet["text"])
      alert_options = { twitter_id: tweet["user"]["id_str"], tweet_id: tweet["id"], screen_name: tweet['user']['screen_name'], message_time: tweet["created_at"], post_source: tweet["source"] }
      tweet_place = tweet["place"].presence
      if tweet_place
        alert_options.merge!(post_place: %Q<#{tweet_place["full_name"] - tweet_place["country_code"]}>)
      end
      begin
        if author.auto_delete && quarantine && DeletedTweet.no_exception(tweet)
          DeletedTweet.create_from_tweet(tweet)
          post_deleted_at = Time.now.utc
        end
      rescue => e
        puts "**** ERROR DELETING: #{e}"
      end
      #begin
      #  author.twitter_client.follow("SecureSocialApp")
      #  dm_message = alert_message + " [#{post_message.truncate(130 - alert_message.length)}]"
      #  twitter_client.create_direct_message(tweet["user"]["screen_name"], dm_message)
      #rescue => e
      #  puts "**** ERROR DM: #{e}"
      #end
    end

    alert_options.merge!(message: post_message, rule_id: id, author_email: author_email, author_phone: author.phone,
                         phishing: phishing, fraud: fraud,
                         group_id: author.group_id, user_id: author.user_id, post_deleted_at: post_deleted_at)

    if send_author
      if author.alert_email
        UserMailer.alert(author_email, "#{social_provider}: " + alert_message, post_message, post_deleted_at.present?, self, raw_message).deliver_later if author_email
        alert_options.merge!(author_email_alerted: true)
      end

      if author.alert_phone && author.phone
        Alert.send_sms(twilio_client, author.phone, "#{alert_message}: #{post_message}")
        alert_options.merge!(author_phone_alerted: true)
      end
    end

    if send_admins
      if author.admin_emails.present?
        author.admin_emails.split(",").each do |admin_email|
          UserMailer.alert_admin(admin_email.strip, "#{social_provider}: " + alert_message, post_message, post_deleted_at.present?).deliver_later
        end
      end

      if author.admin_phones.present?
        author.admin_phones.split(",").each do |admin_phone|
          Alert.send_sms(twilio_client, admin_phone.strip, "Alert Triggered: #{alert_message} - #{post_message}")
        end
      end
      alert_options.merge!(admins_alerted: true)
    end

    if send_phishing_admins
      if author.phishing_alert_emails.present?
        author.phishing_alert_emails.split(",").each do |phishing_admin_email|
          UserMailer.alert_admin(phishing_admin_email.strip, "Phising Alert on #{social_provider}: " + alert_message, post_message, post_deleted_at.present?).deliver_later
        end
      end

      if author.phishing_alert_phones.present?
        author.phishing_alert_phones.split(",").each do |phishing_admin_phone|
          Alert.send_sms(twilio_client, phishing_admin_phone.strip, "Phishing Alert Triggered: #{alert_message} - #{post_message}")
        end
      end
      alert_options.merge!(admins_alerted: true)
    end

    if send_fraud_admins
      if author.fraud_alert_emails.present?
        author.fraud_alert_emails.split(",").each do |fraud_admin_email|
          UserMailer.alert_admin(fraud_admin_email.strip, "Fraud Alert on #{social_provider}: " + alert_message, post_message, post_deleted_at.present?).deliver_later
        end
      end

      if author.fraud_alert_phones.present?
        author.fraud_alert_phones.split(",").each do |fraud_admin_phone|
          Alert.send_sms(twilio_client, fraud_admin_phone.strip, "Fraud Alert Triggered: #{alert_message} - #{post_message}")
        end
      end
      alert_options.merge!(admins_alerted: true)
    end

    Alert.create! alert_options

    if post_deleted_at
      author.twitter_client.destroy_status(tweet["id_str"])
      puts "************** TWEET DELETED: #{tweet["user"]["screen_name"]}/#{tweet["id_str"]} | #{post_message}"
    end
  end

end
