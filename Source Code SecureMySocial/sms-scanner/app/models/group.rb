class Group < ActiveRecord::Base
  has_many :members
  has_many :users, -> { distinct }, through: :members

  has_many :rules
end
