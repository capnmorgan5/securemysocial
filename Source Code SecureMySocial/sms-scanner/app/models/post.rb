class Post < ActiveRecord::Base

  def self.pending
    where(processed_at: nil)
  end

  def masked_message
    return unless message.present?
    "#{message[0,10]}..."
  end
end
