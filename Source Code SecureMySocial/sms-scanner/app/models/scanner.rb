class Scanner

  attr_accessor :rules

  def initialize(rules)
    @rules = rules || []
  end

  def first_match(text)
    return unless text.present?

    normalized_text = text.strip.squeeze(" ")

    rules.each do |rule|
      return rule if rule.normalized_patterns.any? { |pattern| normalized_text.match(/#{pattern}/im) }
    end

    nil
  end

end
