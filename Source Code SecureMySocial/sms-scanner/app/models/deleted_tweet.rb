class DeletedTweet < ActiveRecord::Base

  def self.create_from_tweet(tweet)
    create tweet_id: SecureRandom.uuid.gsub("-", "").hex, id_str: tweet["id_str"], tweet: tweet["text"],
           screen_name: tweet["user"]["screen_name"], tweeted_at: tweet["created_at"],
           deleted_at: Time.now.utc
  end

  def self.no_exception(tweet)
    0 == where(screen_name: tweet["user"]["screen_name"])
        .where("tweet = E#{ActiveRecord::Base.connection.quote(tweet["text"])}")
        .where("deleted_at > current_timestamp - interval '24 hours'").count
  end
end
