class Alert < ActiveRecord::Base

  def self.send_sms(client, phone, message)
    client.messages.create from: '+1 281-825-3787', to: phone, body: message
  end

end
