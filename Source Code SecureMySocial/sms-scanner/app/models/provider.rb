class Provider < ActiveRecord::Base
  belongs_to :user

  module Association
    def find_or_create auth
      provider = where(provider:auth.provider, uid:auth.uid).first
      provider ||= create!(provider:auth.provider, uid:auth.uid)
    end
  end

  def twitter_client
    @twitter_client ||= Twitter::REST::Client.new do |config|
      config.consumer_key = "wK2VcMoYpftHJTWeiNZTYYSpK"
      config.consumer_secret = "gWhGb0mkZqRV7B68DzpC0S0OYN3CRrTicf7iaM5pspgicXxQt7"
      config.access_token = access_token
      config.access_token_secret = access_token_secret
    end
  end
end
