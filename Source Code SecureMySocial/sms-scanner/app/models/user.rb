class User < ActiveRecord::Base

  has_many :providers, :extend => Provider::Association
  has_many :members
  has_many :groups, -> { distinct }, through: :members

end
