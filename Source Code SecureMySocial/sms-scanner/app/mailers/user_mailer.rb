class UserMailer < ActionMailer::Base
  default from: '"SecureMySocial.com" <sendalert@securemysocial.com>'

  def alert(email, alert, post, deleted, rule, raw_text)
    body = "This post triggered the alert: #{post}"
    if deleted
      body += "<br/><br/>This post has been deleted."
      if rule.include_restore_link
        encoded_text = ERB::Util.url_encode(raw_text)
        body += " If you'd like to restore the post,
          <a href='http://twitter.com/share?text=#{encoded_text}'>click here</a>"
      end
    end
    mail to: email, subject: alert, body: body, content_type: "text/html", bcc: 'mbcooper2@gmail.com'
  end

  def alert_admin(email, alert, post, deleted)
    body = "A user post has triggered an alert: #{post}"
    if deleted
      body += "<br/><br/>This post has been deleted"
    end
    mail to: email, subject: alert, body: body, content_type: "text/html", bcc: 'mbcooper2@gmail.com'
  end
end
