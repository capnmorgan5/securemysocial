require 'test_helper'

class ScannerTest < ActiveSupport::TestCase
  test "#first_match" do
    rule = Rule.new(patterns: "The Merger")
    scanner = Scanner.new([rule])
    assert scanner.first_match("let's talk about the merger")
    assert !scanner.first_match("There is no merger talk here")
  end
  test "multi pattern" do
    rule = Rule.new(patterns: "Don't mind me\nThe Merger")
    scanner = Scanner.new([rule])
    assert scanner.first_match("let's talk about the merger")
    assert !scanner.first_match("There is no merger talk here")
  end
  test "repeated letters" do
    rule = Rule.new(patterns: "Application\nassume")
    scanner = Scanner.new([rule])
    assert scanner.first_match("Don't assume anything")
    assert !scanner.first_match("No assumptions here")
  end
  test "extra spaces" do
    rule = Rule.new(patterns: "Application  sucks\nassume")
    scanner = Scanner.new([rule])
    assert scanner.first_match("This application     sucks bigtime")
    assert !scanner.first_match("this application does not suck")
  end
  test "Regular Expressons" do
    rule = Rule.new(patterns: "Who Cares\n\n" + '\d0\d-\d{3}' + "\naoheunoaehu")
    scanner = Scanner.new([rule])
    assert scanner.first_match("000-435")
    assert scanner.first_match("109-435")
    assert scanner.first_match("109-435ahoteu")
    assert scanner.first_match("109-435ahoteu")
    assert !scanner.first_match("344-243")
    assert !scanner.first_match("109-a43")
  end
end
