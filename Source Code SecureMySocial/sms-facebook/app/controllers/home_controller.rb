class HomeController < ApplicationController

  def fb
    return(render(text: Koala::Facebook::RealtimeUpdates.meet_challenge(params, "a42"))) unless request.post?

    begin
      if params[:entry] && (uid = params[:entry][0][:id]).present?
        if (p = Provider.where(uid: uid).first).present?
          fb = Koala::Facebook::API.new(p.access_token)
          statuses = fb.get_connections("me", "statuses", limit: 1)
          Post.record_facebook_post(statuses[0], p.user_id)
        end
      end
    rescue => e
      logger.error e
    end

    render nothing: true
  end

end
