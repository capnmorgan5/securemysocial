class User < ActiveRecord::Base
  has_many :providers, :extend => Provider::Association
end
