require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)

require 'clockwork'
require 'linkedin'

include Clockwork

client = LinkedIn::Client.new("776ttc6lh2ptot", "7S7jFLz32kzeCASd")

def self.log message
  puts "*** #{Time.now} - #{message}"
end

def process(client)
  log "Begin Process"
  
  users_to_scan = Provider.where("provider = 'linkedin'").map { |u| [u.access_token, u.access_token_secret] }.flatten
  puts "LinkedIn Authenticated Users Uids: #{users_to_scan}"
  puts "User 1 - #{users_to_scan[0]}, #{users_to_scan[1]}"

  
  request_token = client.request_token({}, :scope => "r_basicprofile r_emailaddress")
  token, secret = client.authorize_from_access(users_to_scan[0], users_to_scan[1])
  #puts "token - #{token}"
  #puts "secret - #{secret}"

  profile = client.profile
  puts profile.inspect
  #puts request_token.inspect

  #client.authorize_from_access(users_to_scan[0], users_to_scan[1])

  #puts linkedin_client.network_updates

  #updated_users_uids = User.where("updated_at > current_timestamp - interval '30 minutes'").includes(:providers).all.map { |u| u.providers.map(&:uid) }.flatten
  #puts "Updated Users Uids: #{updated_users_uids}"
  
  #Post.record_linkedin_post(statuses[0], p.user_id)

  log "End Process"
end

every(30.seconds, 'Queueing interval job') do
  process(client)
end
